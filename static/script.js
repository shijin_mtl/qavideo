var iosSafari =
  /iP(ad|od|hone)/i.test(window.navigator.userAgent) &&
  /WebKit/i.test(window.navigator.userAgent) &&
  !/(CriOS|FxiOS|OPiOS|mercury)/i.test(window.navigator.userAgent);
var isAndroid = /(android)/i.test(navigator.userAgent);
var isChrome = /chrom(e|ium)/.test(navigator.userAgent.toLowerCase());
var isMobile = window.orientation > -1;
var isUC = navigator.userAgent.match(/UCBrowser/i);
var isOpera = navigator.userAgent.toLowerCase().indexOf("op") > -1;
var isFirefox = navigator.userAgent.indexOf("Firefox") > -1;
var isEdge = navigator.userAgent.indexOf("Edg") > -1;
var isRecording = false;
var recorder;
var urls = [];
var localStem;
var currentValueId = "name";
var isSumbitted = false;
var formAudio = new Audio();
var languageForm = "en";

var localJson = {
  en: {
    name: {
      currentId: "name",
      audio:
        "https://uiresource.blob.core.windows.net/chatbot-res/irctc/res/name.mp3",
      noSpace: false,
      nextId: "phonenumber",
    },
    phonenumber: {
      currentId: "phonenumber",
      audio:
        "https://uiresource.blob.core.windows.net/chatbot-res/irctc/res/phonenumber.mp3",
      noSpace: true,
      nextId: "email",
    },
    email: {
      currentId: "email",
      audio:
        "https://uiresource.blob.core.windows.net/chatbot-res/irctc/res/email.mp3",
      noSpace: true,
      nextId: null,
    },
    submitAudio:
      "https://uiresource.blob.core.windows.net/chatbot-res/irctc/res/submit.mp3",
  },
  hi: {
    name: {
      currentId: "name",
      audio:
        "https://uiresource.blob.core.windows.net/chatbot-res/irctc/res/name_hi.mp3",
      noSpace: false,
      nextId: "phonenumber",
    },
    phonenumber: {
      currentId: "phonenumber",
      audio:
        "https://uiresource.blob.core.windows.net/chatbot-res/irctc/res/phone_hi.mp3",
      noSpace: true,
      nextId: "email",
    },
    email: {
      currentId: "email",
      audio:
        "https://uiresource.blob.core.windows.net/chatbot-res/irctc/res/email_hi.mp3",
      noSpace: true,
      nextId: null,
    },
    submitAudio:
      "https://uiresource.blob.core.windows.net/chatbot-res/irctc/res/submit_hi.mp3",
  },
};

if (isUC || isOpera || isFirefox || isEdge) {
  iosSafari = false;
  isChrome = false;
}

if (true) {
  var get_lead = false;
  var medtel_live = false;

  const SpeechRecognition =
    window.SpeechRecognition || window.webkitSpeechRecognition;

  $(window).on("load", hidePreloader);
  $(".thinking-modal").hide();

  function hidePreloader() {
    $(".Hungama_widgetBox").hide();
    $("#videooff").hide();
    $("#videoOn").hide();
    $(".preloader").hide();
    // hide user cam onload
    $("#user").hide();
    $("#trendingSection").hide();
    $("#show").hide();
    $("#loader-wrapper").hide();
    $("#linkInner").hide();
  }

  var closebtn = document.querySelector("#closebtnfeed");
  closebtn.addEventListener("click", function () {
    get_lead = false;
    $(".lead-container").addClass("hideThis");
    $(".medtel").removeClass("hideThis");
    $(".nextInput").addClass("hideThis");
    $(".listeningForm").addClass("hideThis");
    formAudio.pause();
  });

  function micSuccess(stream) {
    checkOM();
  }

  function micFail(error) {
    $(".corover-start").addClass("hideThis");
    $(".mic-modal").removeClass("hideThis");
  }

  function openFullscreen(elem) {
    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem.mozRequestFullScreen) {
      /* Firefox */
      elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) {
      /* Chrome, Safari and Opera */
      elem.webkitRequestFullscreen();
    } else if (elem.msRequestFullscreen) {
      /* IE/Edge */
      elem.msRequestFullscreen();
    }
  }
  let isListening = false;
  let shownTut = false;
  let handleUnknown = "assets/noAns.mp4";

  $(".micBtn").click(function () {

    let isChrome =
      /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
    let isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    if (isChrome && isMobile == false) {
      startRecording();
      console.log("start recording");
    }
    medtel_live = false;
    $(".popup").addClass("hideThis");
    $(".medtel").addClass("hideThis");
    if (!isListening) {
      isListening = true;
      $(".arrow").addClass("hideThis");
      startSpeech();
      $(".video-response").get(0).src = "";
    }
  });

  $("#corover-start-btn").click(function () {
    videoOn();
    $(".Hungama_widgetBox").remove();
    $("#supportText").hide();
    $(".owl-carousel").hide();
    $("#latestinfo").removeClass("hideThis");
    $(".aboutCorona").hide();
    $("#linkOuter").hide();
    $("#linkInner").show();
    $(".npciLogo").hide();
    trending();
    $("#videooff").show();
    if (!!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform)) {
      $(".micBtn").css("display", "none");
    }
    open_close_trending();
    $(".ios_mobile").addClass("hideThis");
    $(".corover-start").addClass("hideThis");
    $(".video-response").removeClass("hideThis");
    // $('.npci-logo').addClass('hideThis');
    if (isMobile) {
      $(".video-response").get(0).src = "assets/intro_mobile.mp4";
    } else {
      $(".video-response").get(0).src = "assets/intro_desktop.mp4";
    }
    $(".video-response").get(0).play();
    openFullscreen(document.body);
  });

  // *** When video finishes playing ***
  $(".video-response").on("ended", function () {
    $(".video-response").get(0).src = "";
    $(".idle").get(0).play();
    $(".video-response").addClass("hideThis");
    $("#talk_text").text("")
    if (!!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform)) {
      $(".iosMic").removeClass("hideThis");
    }
    if (get_lead) {
      $(".lead-container").removeClass("hideThis");
    }
    if (medtel_live === true) {
      $(".medtel").removeClass("hideThis");
    }
    // $('.thinking-modal').addClass('hideThis');
    $(".idle").removeClass("hideThis");
    // $('.micBtn').css('width', '20%');
    if (!shownTut) {
      shownTut = true;
      $(".arrow").removeClass("hideThis");
    }
    $(".micBtn")[0].src = "/static/Mic.svg";
    if (!$(".micBtn").hasClass("showMic")) {
      $(".micBtn").removeClass("hideThis");
      setTimeout(function () {
        $(".micBtn").addClass("showMic");
      }, 100);
    }
  });

  function open_close_trending() {
    if (window.innerHeight > window.innerWidth) {
      // console.log('portrait css');
      $(".navbar").css("width", "70%");
      $(".link-text").css("display", "inline");
      $("#show").hide();
    } else {
      $(".navbar").css("width", "30%");
      $(".link-text").css("display", "inline");
      $("#show").hide();
    }
    setTimeout(() => {
      $(".navbar").css("width", "0rem");
      $(".link-text").css("display", "none");
      $("#show").fadeIn(2000);
    }, 3000);
  }

  // *** Play video when it is playable ***
  function checkBuffer() {
    // setTimeout(function () {
    // // console.log(document.querySelector('.video-response').buffered.end(0), document.querySelector('.video-response').duration);
    // },1000);
    // setTimeout(function () {
    // if (document.querySelector('.video-response').buffered.end(0) / document.querySelector('.video-response').duration * 100 >= 5) {
    //     $('.idle').addClass('hideThis');
    //     $('.video-response').removeClass('hideThis');
    //     $('.video-response').get(0).play();
    // }
    // }, 2000);

    setTimeout(function () {
      document.querySelector(".video-response").oncanplay = playVideo();
      // playVideo();
    }, 2500);
  }

  // *** Function to play video ***
  function playVideo() {
    alert("innnn")
    // console.log('bot speak');
    // $('.thinking-modal').addClass('hideThis');
    $(".micBtn").removeClass("hideThis");
    // $('.idle').addClass('hideThis');
    $(".video-response").removeClass("hideThis");
    botSpeak();
    $(".video-response").get(0).play();
  }

  // API body for fetch
  var firstname;
  var phonenumber;
  var email;
  var setQuestionRecordAPI = "/corona/getAnswer?languageCode=en";
  var leadgenAPI = "/corona/saveAdditionInfo?languageCode=en";
  var answerWithVoice = "/corona/getAnswerWithAudio?languageCode=en";
  var text_audio = document.createElement("audio");

  check_cookie();

  var setQuestionRecordBody = {
    query: "hi",
    channel: "browser | Videobot User",
    inputType: "VOICE",
  };

  var lead_param = {
    query: "hi",
    channel: "browser | Videobot User",
    fields: [
      {
        label: "name",
        value: "Rafique",
        type: "TEXT",
      },
      {
        label: "email",
        value: "rafique@corover.mobi",
        type: "TEXT",
      },
      {
        label: "phone",
        value: "9988776655",
        type: "TEXT",
      },
    ],
  };
  var url = window.location.href;

  if (isMobile) {
    lead_param.channel = "Videobot | Mobile User";
    setQuestionRecordBody.channel = "Videobot | Mobile User";
  } else if (isMobile && url.indexOf("?channel=android") != -1) {
    lead_param.channel = "Videobot | Android APP";
    setQuestionRecordBody.channel = "Videobot | Android APP";
  } else if (isMobile && url.indexOf("?channel=ios") != -1) {
    lead_param.channel = "Videobot | iOS APP";
    setQuestionRecordBody.channel = "Videobot | iOS APP";
  } else {
    lead_param.channel = "Videobot | Desktop User";
    setQuestionRecordBody.channel = "Videobot | Desktop User";
  }

  // *** Send API Logic ***
  function sendAPI(transcript) {
    setQuestionRecordBody.query = transcript;
    $.ajax({
      url: "/detect/",
      method: "POST",
      data: JSON.stringify(setQuestionRecordBody),
      beforeSend: function (xhr) {
        xhr.setRequestHeader("content-type", "application/json");
      },
    })
      .done(function (data) {
        // data = {"id":"61d7eabf-d42b-49ad-922c-49e4ca8e1cc8","reply":"Q29yb25hdmlydXNlcyBhcmUgYSBsYXJnZSBmYW1pbHkgb2YgdmlydXNlcyB3aGljaCBtYXkgY2F1c2UgaWxsbmVzcyBpbiBhbmltYWxzIG9yIGh1bWFucy4gSW4gaHVtYW5zLCBzZXZlcmFsIGNvcm9uYXZpcnVzZXMgYXJlIGtub3duIHRvIGNhdXNlIHJlc3BpcmF0b3J5IGluZmVjdGlvbnMgcmFuZ2luZyBmcm9tIHRoZSBjb21tb24gY29sZCB0byBtb3JlIHNldmVyZSBkaXNlYXNlcyBzdWNoIGFzIE1pZGRsZSBFYXN0IFJlc3BpcmF0b3J5IFN5bmRyb21lIChNRVJTKSBhbmQgU2V2ZXJlIEFjdXRlIFJlc3BpcmF0b3J5IFN5bmRyb21lIChTQVJTKS4gVGhlIG1vc3QgcmVjZW50bHkgZGlzY292ZXJlZCBjb3JvbmF2aXJ1cyBjYXVzZXMgY29yb25hdmlydXMgZGlzZWFzZSBDT1ZJRC0xOS4=","audio":"https://coroverbackendstorage.blob.core.windows.net/chatbot-audio-bucket/cda1cc65-17d5-4a3f-8a60-dd40f02d4abd_en.mp3","desktopVideo":"https://coroverbackendstorage.blob.core.windows.net/chatbot-video-bucket/desktop_Intent1.mp4","mobileVideo":"https://coroverbackendstorage.blob.core.windows.net/chatbot-video-bucket/mobile_Intent1.mp4","news":false}
        if (data.news == false || data.news == undefined) {
          const base64Rejex = /^(?:[A-Z0-9+\/]{4})*(?:[A-Z0-9+\/]{2}==|[A-Z0-9+\/]{3}=|[A-Z0-9+\/]{4})$/i;
          const isBase64Valid = base64Rejex.test(data.reply); // base64Data is the base64 string
          text_audio.src = data.audio;
          if (isBase64Valid) {
            // true if base64 formate
            data.reply = decodeURIComponent(escape(window.atob(data.reply)));
          }

          var cardAnswer = document.querySelector("#textAnswer");
          cardAnswer.innerHTML = data.reply;

          if (isListening) {
            isListening = false;
            $(".micBtn")[0].src = "/static/Mic.svg";
            $(".micBtn").addClass("hideThis");

            if (
              data.desktopVideo === "" ||
              data.desktopVideo === undefined ||
              data.desktopVideo === null
            ) {
              get_lead = false;
              $(".video-response").get(0).src =
                "https://coroverbackendstorage.blob.core.windows.net/iglcontainer/desktop_PR.mp4";
              $(".video-response").get(0).play();
              $(".popup").removeClass("hideThis");
              checkbuff();
            } else {
              get_lead = false;
              $(".video-response").get(0).src = data.desktopVideo;
              $(".video-response").get(0).play();
              checkbuff();
            }
          }
        } else {
          isListening = false;
          if (cookie) {
            get_lead = false;
            medtel_live = true;
            $(".lead-container").addClass("hideThis");
            $(".video-response").get(0).src =
              "https://coroverbackendstorage.blob.core.windows.net/chatbot-video-bucket/desktop_FBR1.mp4";
            sendLeadGeneration(firstname, phonenumber, email);
            $(".video-response").get(0).play();
            checkbuff();
          } else {
            get_lead = true;
            $(".video-response").get(0).src = handleUnknown;
            $(".video-response").get(0).play();
            checkbuff();
          }
        }
      })
      .fail(function (xhr, status, error) {
        $(".thinking-modal").hide();
        if (isListening) {
          isListening = false;

          $(".micBtn")[0].src = "/static/Mic.svg";
          $(".micBtn").addClass("hideThis");
          // $('.micBtn').css('width', '20%');

          if (cookie) {
            get_lead = false;
            medtel_live = true;
            $(".lead-container").addClass("hideThis");
            $(".video-response").get(0).src =
              "https://coroverbackendstorage.blob.core.windows.net/chatbot-video-bucket/desktop_FBR1.mp4";
            sendLeadGeneration(firstname, phonenumber, email);
            $(".video-response").get(0).play();
            checkbuff();
          } else {
            get_lead = true;
            $(".video-response").get(0).src = handleUnknown;
            $(".video-response").get(0).play();
            checkbuff();
          }
        }
      });
  }

  $("#button-blue").click(function (e) {
    
  });

  function sendLeadGeneration(name, number, email) {
    
  }

  // *** Voice recognition Logic ***
  function startSpeech() {
    let transcript;

    if (window.hasOwnProperty("webkitSpeechRecognition")) {
      var recognition = new webkitSpeechRecognition();

      recognition.continuous = false;
      recognition.interimResults = false;

      recognition.lang = "en-US";

      if (isListening) {
        recognition.start();
      }

      // Error handling
      recognition.onerror = function (e) {
        if (e.error == "not-allowed") {
          $(".corover-start").addClass("hideThis");
          $(".mic-modal").removeClass("hideThis");
        } else if (e.error == "aborted") {
          isListening = false;
          $(".video-response").get(0).src =
            "https://coroverbackendstorage.blob.core.windows.net/chatbot-video-bucket/desktop_FBR1.mp4";
          $(".video-response").get(0).play();
          checkbuff();
        }
      };

      recognition.onstart = function () {
        $(".micBtn")[0].src = "/static/Micactive.svg";
        // console.log('Recognition started');
        $(".idle").removeClass("hideThis");
      };

      // On Result
      recognition.onresult = function (e) {
        transcript = Array.from(e.results)
          .map((result) => result[0])
          .map((result) => result.transcript)
          .join(" ");


        if (!transcript) {
        }

        recognition.stop();
      };

      // On recognition end
      recognition.onend = function () {
        $(".micBtn")[0].src = "/static/Mic.svg";
        if (isChrome && isMobile) {
          // $('.thinking-modal').removeClass('hideThis');
          if (transcript && transcript !== "") {
            sendAPI(transcript.toLowerCase());
          } else {
            if (isListening) {
              isListening = false;
              $(".video-response").get(0).src =
                "https://coroverbackendstorage.blob.core.windows.net/chatbot-video-bucket/desktop_FBR1.mp4";
              $(".video-response").get(0).play();
              checkbuff();
            }
          }
          // console.log('Recognition stopped');
          setTimeout(function () {
            $("#closeUserCam").show();
          }, 500);
        } else {
          stopRecording(transcript.toLowerCase());
        }
      };
    }
  }

  function voiceCapture(transcript, blob) {
    setQuestionRecordBody.query = transcript;

    var formData = new FormData();
    formData.append("questionData", JSON.stringify(setQuestionRecordBody));
    formData.append("file", blob);

    $("#talk_text").text(JSON.stringify(transcript))

    $.ajax({
      url: "/detect/",
      method: "POST",
      data: formData,
      processData: false,
      contentType: false,
      beforeSend: function (xhr) {
        
      },
    }).done(function (data) {
      // data = {"id":"61d7eabf-d42b-49ad-922c-49e4ca8e1cc8","reply":"Q29yb25hdmlydXNlcyBhcmUgYSBsYXJnZSBmYW1pbHkgb2YgdmlydXNlcyB3aGljaCBtYXkgY2F1c2UgaWxsbmVzcyBpbiBhbmltYWxzIG9yIGh1bWFucy4gSW4gaHVtYW5zLCBzZXZlcmFsIGNvcm9uYXZpcnVzZXMgYXJlIGtub3duIHRvIGNhdXNlIHJlc3BpcmF0b3J5IGluZmVjdGlvbnMgcmFuZ2luZyBmcm9tIHRoZSBjb21tb24gY29sZCB0byBtb3JlIHNldmVyZSBkaXNlYXNlcyBzdWNoIGFzIE1pZGRsZSBFYXN0IFJlc3BpcmF0b3J5IFN5bmRyb21lIChNRVJTKSBhbmQgU2V2ZXJlIEFjdXRlIFJlc3BpcmF0b3J5IFN5bmRyb21lIChTQVJTKS4gVGhlIG1vc3QgcmVjZW50bHkgZGlzY292ZXJlZCBjb3JvbmF2aXJ1cyBjYXVzZXMgY29yb25hdmlydXMgZGlzZWFzZSBDT1ZJRC0xOS4=","audio":"https://coroverbackendstorage.blob.core.windows.net/chatbot-audio-bucket/cda1cc65-17d5-4a3f-8a60-dd40f02d4abd_en.mp3","desktopVideo":"https://media.istockphoto.com/videos/overjoyed-woman-make-videocall-chatting-with-relatives-feels-happy-video-id1190928923","mobileVideo":"https://media.istockphoto.com/videos/overjoyed-woman-make-videocall-chatting-with-relatives-feels-happy-video-id1190928923","news":false}
        if (data.news == false || data.news == undefined) {
          const base64Rejex = /^(?:[A-Z0-9+\/]{4})*(?:[A-Z0-9+\/]{2}==|[A-Z0-9+\/]{3}=|[A-Z0-9+\/]{4})$/i;
          const isBase64Valid = base64Rejex.test(data.reply); // base64Data is the base64 string
          text_audio.src = data.audio;
          if (isBase64Valid) {
            // true if base64 formate
            data.reply = decodeURIComponent(escape(window.atob(data.reply)));
          }

          var cardAnswer = document.querySelector("#textAnswer");
          cardAnswer.innerHTML = data.reply;

          if (isListening) {
            isListening = false;
            // console.log("success");
            $(".micBtn")[0].src = "/static/Mic.svg";
            $(".micBtn").addClass("hideThis");
            // $('.micBtn').css('width', '20%');

            // If saying bye
            if (
              data.desktopVideo === "" ||
              data.desktopVideo === undefined ||
              data.desktopVideo === null
            ) {
              get_lead = false;
              $(".video-response").get(0).src =
                "https://coroverbackendstorage.blob.core.windows.net/iglcontainer/desktop_PR.mp4";
              $(".video-response").get(0).play();
              $(".popup").removeClass("hideThis");
              checkbuff();
            } else {
              get_lead = false;
              $(".video-response").get(0).src = data.desktopVideo;
              $(".video-response").get(0).play();
              checkbuff();
            }
            // $('.video-response').get(0).play();
            // document.getElementById('subtitle').innerHTML = data.answers[0];
          }
        } else {
          isListening = false;
          if (cookie) {
            get_lead = false;
            medtel_live = true;
            $(".lead-container").addClass("hideThis");
            $(".video-response").get(0).src =
              "https://coroverbackendstorage.blob.core.windows.net/chatbot-video-bucket/desktop_FBR1.mp4";
            sendLeadGeneration(firstname, phonenumber, email);
            $(".video-response").get(0).play();
            checkbuff();
          } else {
            get_lead = true;
            $(".video-response").get(0).src = handleUnknown;
            $(".video-response").get(0).play();
            checkbuff();
          }
          // $(".micBtn")[0].src = "Mic.svg";
          // $(".micBtn").addClass("hideThis");
        }
      })
      .fail(function (xhr, status, error) {
        $(".thinking-modal").hide();
        if (isListening) {
          isListening = false;

          $(".micBtn")[0].src = "/static/Mic.svg";
          $(".micBtn").addClass("hideThis");
          // $('.micBtn').css('width', '20%');

          if (cookie) {
            get_lead = false;
            medtel_live = true;
            $(".lead-container").addClass("hideThis");
            $(".video-response").get(0).src =
              "https://coroverbackendstorage.blob.core.windows.net/chatbot-video-bucket/desktop_FBR1.mp4";
            sendLeadGeneration(firstname, phonenumber, email);
            $(".video-response").get(0).play();
            checkbuff();
          } else {
            get_lead = true;
            $(".video-response").get(0).src = handleUnknown;
            $(".video-response").get(0).play();
            checkbuff();
          }
        }
      });
  }
  function check_cookie() {
    firstname = Cookies.get("username_fortis");
    phonenumber = Cookies.get("number_fortis");
    email = Cookies.get("email_fortis");

    if (firstname === undefined) {
      cookie = false;
    } else {
      cookie = true;
    }
  }
  // trending question
  function trending() {
    $("#trendingSection").show();
    $("#show").show();

    // var header = document.createElement("h1");
    // header.innerText = "Trending Question!!!!!";
    // var items = document.getElementById('trending');
    // items.appendChild(header);
    var ul = document.getElementById("trendparent");
    var trending = [
      "What is a coronavirus?",
      "What is covid-19?",
      "what are the symptoms of covid-19?",
      "How does covid-19 spread?",
      "Can the virus that causes COVID-19 be transmitted through the air?",
      "Can CoVID-19 be caught from a person who has no symptoms?",
      "How likely am I to catch COVID-19?",
      "Who is at risk of developing severe illness?",
      "Are antibiotics effective in preventing or treating the COVID-19?",
      "Are there any medicines or therapies that can prevent or cure COVID-19?",
      "Is there a vaccine, drug or treatment for COVID-19?",
      "Should I wear a mask to protect myself?",
      "How to put on, use, take off and dispose of a mask?",
      "How long is the incubation period for COVID-19?",
      "Can humans become infected with the COVID-19 from an animal source?",
      "How long does the virus survive on surfaces?",
      "Is it safe to receive a package from any area where COVID-19 has been reported?",
      "Is there anything I should not do?",
      "What is your advice on Travel?",
      "Does hot & humid climate prevent coronavirus infection?",
      "Can cold weather and snow kill coronavirus infection?",
      "Can this infection spread by mosquitoes?",
      "Can hand dryer kill coronavirus?",
      "Can spraying alcohol and chlorine all over the body kill the coronavirus?",
      "Can regularly rinsing the nose with saline water prevents the coronavirus infection?",
      "Is it safe to eat non-vegetarian food like fish, mutton/meat, chicken, prawns/shrimp, pork etc.?",
      "Can eating garlic help prevent infection with the new coronavirus?",
      "Is it safe to drink alcohol and continue to smoke during coronavirus infection?",
      "Can drinking hot water help prevent coronavirus infection?",
    ];
    trending.forEach(function (Q) {
      // // console.log(Q);
      var item = document.createElement("li");
      item.setAttribute("class", "nav-item");
      item.innerHTML = `

                            <a href="#" class="nav-link">
                            <i class="fa fa-cube " aria-hidden="true" style="margin-left:10px;"></i>
                            <span class="link-text">${Q}</span>
                            </a>

                        `;
      ul.appendChild(item);
    });
    $(".link-text").on("click", function () {
      $(".navbar").css("width", "0rem");
      $("#show").fadeIn(2000);
      $(".thinking-modal").show();
      // $('#loader-wrapper').show();
      $(".idle").removeClass("hideThis");
      $(".video-response").addClass("hideThis");
      // console.log('question clicked');
      // console.log(this.innerText);
      senapi(this.innerText);

      function senapi(transcript) {
        $(".thinking-modal").show();
        setQuestionRecordBody.query = transcript;
        $.ajax({
          url: setQuestionRecordAPI,
          method: "POST",
          data: JSON.stringify(setQuestionRecordBody),
          beforeSend: function (xhr) {
            xhr.setRequestHeader(
              "partner-key",
              "006134668452862086110:j7ihqcuf4xz"
            );
            xhr.setRequestHeader(
              "app-id",
              "7cb9605c-692b-11ea-bc55-0242ac130003"
            );
            xhr.setRequestHeader(
              "auth-key",
              "8a16dfcc-692b-11ea-bc55-0242ac130003"
            );
            xhr.setRequestHeader("content-type", "application/json");
          },
        }).done(function (data) {
          // videoUrl = data.desktopVideo;
          // transcript = data.answers[0];
          // $('.idle').addClass('hideThis');
          // console.log(data.desktopVideo);
          $(".video-response").get(0).src = data.desktopVideo;
          $(".video-response").get(0).play();
          checkbuff();
        });
        // console.log('nice');
      }
      // $('.video-response').removeClass('hideThis');
      // $('.idle').addClass('hideThis');
      // setTimeout(function() {
      //     $('body').addClass('loaded');
      // }, 1000);
      // $('body').removeClass('loaded');
    });
  }

  function checkbuff() {
    var vid = document.getElementById("vid");
    // // console.log(document.querySelector('.video-response').buffered.end(0), document.querySelector('.video-response').duration);
    // console.log()
    var buffered = function () {
      var bufferedPercent =
        vid.duration > 0 && vid.buffered.length > 0
          ? (vid.buffered.end(0) / vid.duration) * 10
          : 0;
      return "buffered " + bufferedPercent.toFixed(0) + "%";
    };

    vid.onprogress = function () {
      // console.log('progress: ' + buffered());
    };

    vid.oncanplay = function () {
      $(".thinking-modal").hide();
      $(".idle").addClass("hideThis");
      $(".video-response").removeClass("hideThis");
      $(".video-response").fadeIn("slow");
      // botspeak
      botSpeak();
      $(".micBtn").removeClass("hideThis");
      // $('.idle').addClass('hideThis');
      shownTut = false;
      // console.log('canplay: ' + buffered());
    };

    vid.oncanplaythrough = function () {
      // console.log('canplaythrough: ' + buffered());
    };

    vid.onsuspend = function () {
      // console.log('suspend: ' + buffered());
    };
  }

  // trending section
  $("#show").hover(function () {
    if (window.innerHeight > window.innerWidth) {
      // console.log('portrait css');
      $(".navbar").css("width", "70%");
      $(".link-text").css("display", "inline");
      $("#show").hide();
    } else {
      $(".navbar").css("width", "30%");
      $(".link-text").css("display", "inline");
      $("#show").hide();
    }
  });

  $("#closebtnmedtel").on("click", function () {
    medtel_live = false;
    $(".medtel").addClass("hideThis");
  });
  $(".closetrend").on("click", function () {
    // console.log('click');
    $(".navbar").css("width", "0rem");
    $(".link-text").css("display", "none");
    $("#show").fadeIn(2000);
  });

  $(".navbar").mouseleave(function () {
    // console.log('mouse left');
    $(".navbar").css("width", "0px");
    $("#show").fadeIn(2000);
  });

  // function play_voice() {
  //     $('.ply-btn').addClass('hideThis');
  //     $('.stp-btn').removeClass('hideThis');
  //     text_audio.play();

  // }

  // function stop_voice() {
  //     $('.ply-btn').removeClass('hideThis');
  //     $('.stp-btn').addClass('hideThis');
  //     text_audio.pause()
  //     text_audio.currentTime = 0
  // }

  function close_reply() {
    $(".popup").addClass("hideThis");
    $(".micBtn").removeClass("hideThis");
    text_audio.pause();
    text_audio.currentTime = 0;
  }

  function recordEvent(className) {
    formAudio.src = localJson[languageForm][className].audio;
    formAudio.play();

    formAudio.onended = () => {
      recordedValue(className);
    };
  }

  function recordedValue(className) {
    $(".nextInput").addClass("hideThis");
    $(".form1").addClass("hideThis");
    $(".listeningForm").removeClass("hideThis");
    let transcript;
    // let inputId = className;

    if (window.hasOwnProperty("webkitSpeechRecognition")) {
      var recognition = new webkitSpeechRecognition();

      recognition.continuous = false;
      recognition.interimResults = false;

      recognition.lang = "en-US";

      recognition.start();
      // Error handling
      recognition.onerror = function (e) {
        if (e.error == "not-allowed") {
          $(".nextInput").removeClass("hideThis");
          $(".listeningForm").addClass("hideThis");
        } else if (e.error == "aborted") {
          recordEvent(localJson[languageForm][currentValueId].nextId);
          $(".nextInput").removeClass("hideThis");
          $(".listeningForm").addClass("hideThis");
        }
      };

      recognition.onstart = function () {
        // $(".micBtn")[0].src = "Micactive.svg";
        // // console.log('Recognition started');
        // $(".idle").removeClass("hideThis");
      };

      // On Result
      recognition.onresult = function (e) {
        transcript = Array.from(e.results)
          .map((result) => result[0])
          .map((result) => result.transcript)
          .join(" ");
        console.log(transcript);

        if (!transcript) {
          // console.log('none');
        }

        recognition.stop();
      };

      // On recognition end
      recognition.onend = function () {
        // $('.thinking-modal').removeClass('hideThis');
        if (transcript && transcript !== "") {
          currentValueId = className;
          if (currentValueId === "name") {
            document.getElementById(className).value = transcript;
            $("#formValue").val(transcript);
          } else if (currentValueId === "email") {
            console.log(transcript);
            document.getElementById(className).value = transcript
              .toLowerCase()
              .replace(/\s/g, "")
              .replace(/daud|dot/g, ".")
              .replace(/attherate|at|ad/g, "@");
            $("#formValue").val(
              transcript
                .toLowerCase()
                .replace(/\s/g, "")
                .replace(/daud|dot/g, ".")
                .replace(/attherate|at|ad/g, "@")
            );
          } else {
            document.getElementById(
              className
            ).value = transcript.toLowerCase().replace(/\s/g, "");
            $("#formValue").val(transcript.toLowerCase().replace(/\s/g, ""));
          }
          $(".nextInput").removeClass("hideThis");
          $(".listeningForm").addClass("hideThis");
        } else {
          recordEvent(currentValueId);
          $(".nextInput").removeClass("hideThis");
          $(".listeningForm").addClass("hideThis");
        }
      };
    }
  }

  $("#formValue").change(function () {
    document.getElementById(currentValueId).value = $("#formValue").val();
  });
  $("#languageForm").change(function () {
    languageForm = $("#languageForm").val();

    if (languageForm == "hi") {
      $("[name=name]").attr("placeholder", "नाम");
      $("[name=email]").attr("placeholder", "ईमेल");
      $("[name=phonenumber]").attr("placeholder", "फ़ोन नंबर");
      $("#button-blue").val("सब्मिट");
      document.getElementById("speakForm").innerHTML = "कृपया बोलें ..";
      document.getElementById("didMean").innerHTML = "क्या आपका यह मतलब था?";
      $("#ttt").attr("data-on", "आवाज़");
      $("#ttt").attr("data-off", "टेक्स्ट");
    } else {
      $("[name=name]").attr("placeholder", "Name");
      $("[name=email]").attr("placeholder", "Email");
      $("[name=phonenumber]").attr("placeholder", "Phone Number");
      $("#button-blue").val("Submit");
      document.getElementById("speakForm").innerHTML = "Please Speak..";
      document.getElementById("didMean").innerHTML = "Did you mean?";
      $("#ttt").attr("data-on", "Voice");
      $("#ttt").attr("data-off", "Text");
    }
  });

  $("#color_mode").on("change", function () {
    colorModePreview(this);
    // console.log("true");
    // recordEvent("name");
  });

  function colorModePreview(ele) {
    if ($(ele).prop("checked") == true) {
      $("body").addClass("dark-preview");
      $("body").removeClass("white-preview");

      recordEvent("name");
    } else if ($(ele).prop("checked") == false) {
      $("body").addClass("white-preview");
      $("body").removeClass("dark-preview");
      $(".nextInput").addClass("hideThis");
      $(".form1").removeClass("hideThis");
      formAudio.pause();
    }
  }
  function successInput() {
    if (localJson[languageForm][currentValueId].nextId === null) {
      $(".nextInput").addClass("hideThis");
      $(".form1").removeClass("hideThis");
      let audio = new Audio();
      audio.src = localJson[languageForm]["submitAudio"];
      audio.play();
    } else {
      $(".nextInput").addClass("hideThis");
      $(".form1").removeClass("hideThis");
      recordEvent(localJson[languageForm][currentValueId].nextId);
    }
  }

  function notSuccessInput() {
    recordEvent(localJson[languageForm][currentValueId].currentId);
  }

  var prevTextboxWords = "";
  var textboxWords = "";
  var textRepeatTimes = 0;
  var prevTextCheck = "";
  var textCheck = "";
  var textCheckNumber = 0;
  var x = 0;
  var speechTimeOutLimit = 16;
  var iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
  if (iOS) {
    coroverIosVoice();

    function coroverIosVoice() {
      setTimeout(function () {
        textCheck = $("#record").val();
        var numberOfWordsSpoken = textCheck.split(" ").length;
        if (numberOfWordsSpoken < 3) speechTimeOutLimit = 16;
        else if (numberOfWordsSpoken < 6) speechTimeOutLimit = 12;
        else speechTimeOutLimit = 7;
        if (textCheck != "" && textCheck == prevTextCheck) {
          textCheckNumber++;
        } else {
          textCheckNumber = 0;
        }
        if (textCheck.length < 4) textCheckNumber = 0;
        if (textCheckNumber > speechTimeOutLimit) {
          textCheckNumber = 0;
          $("#record").val("");
          prevTextCheck = "";
          textCheck = textCheck.toLowerCase();

          // console.log(textCheck + "HIT");

          setQuestionRecordBody.query = textCheck;
          $.ajax({
            url: setQuestionRecordAPI,
            method: "POST",
            data: JSON.stringify(setQuestionRecordBody),
            beforeSend: function (xhr) {
              xhr.setRequestHeader(
                "partner-key",
                "006134668452862086110:j7ihqcuf4xz"
              );
              xhr.setRequestHeader(
                "app-id",
                "7cb9605c-692b-11ea-bc55-0242ac130003"
              );
              xhr.setRequestHeader(
                "auth-key",
                "8a16dfcc-692b-11ea-bc55-0242ac130003"
              );
              xhr.setRequestHeader("content-type", "application/json");
            },
          })

            .done(function (data) {
              if (data.news == false || data.news == undefined) {
                const base64Rejex = /^(?:[A-Z0-9+\/]{4})*(?:[A-Z0-9+\/]{2}==|[A-Z0-9+\/]{3}=|[A-Z0-9+\/]{4})$/i;
                const isBase64Valid = base64Rejex.test(data.reply); // base64Data is the base64 string
                text_audio.src = data.audio;
                if (isBase64Valid) {
                  // true if base64 formate
                  data.reply = decodeURIComponent(
                    escape(window.atob(data.reply))
                  );
                }

                var cardAnswer = document.querySelector("#textAnswer");
                cardAnswer.innerHTML = data.reply;
                // videoUrl = data.desktopVideo;
                // transcript = data.answers[0];
                // $('.idle').addClass('hideThis');
                if (
                  data.desktopVideo === "" ||
                  data.desktopVideo === undefined ||
                  data.desktopVideo === null
                ) {
                  get_lead = false;
                  $(".video-response").get(0).src =
                    "https://coroverbackendstorage.blob.core.windows.net/iglcontainer/desktop_PR.mp4";
                  $(".video-response").get(0).play();
                  $(".popup").removeClass("hideThis");
                  checkbuff();
                } else {
                  $(".video-response").get(0).src = data.desktopVideo;
                  $(".video-response").get(0).play();
                  checkbuff();
                }
              } else {
                isListening = false;
                if (cookie) {
                  medtel_live = true;
                  get_lead = false;
                  $(".lead-container").addClass("hideThis");
                  $(".video-response").get(0).src =
                    "https://coroverbackendstorage.blob.core.windows.net/chatbot-video-bucket/desktop_FBR1.mp4";
                  $(".video-response").get(0).play();
                  sendLeadGeneration(firstname, phonenumber, email);
                  checkbuff();
                } else {
                  get_lead = true;
                  $(".video-response").get(0).src =
                    "https://coroverbackendstorage.blob.core.windows.net/iglcontainer/noAns.mp4";
                  $(".video-response").get(0).play();
                  checkbuff();
                }
              }
              $("#record").blur();
            })
            .fail(function (xhr, status, error) {
              if (cookie) {
                medtel_live = true;
                get_lead = false;
                $(".video-response").get(0).src =
                  "https://coroverbackendstorage.blob.core.windows.net/chatbot-video-bucket/desktop_FBR1.mp4";
                $(".video-response").get(0).play();
                sendLeadGeneration(firstname, phonenumber, email);
                $(".lead-container").addClass("hideThis");
                checkbuff();
              } else {
                get_lead = true;
                $(".video-response").get(0).src =
                  "https://coroverbackendstorage.blob.core.windows.net/iglcontainer/noAns.mp4";
                $(".video-response").get(0).play();
                checkbuff();
              }
              // $('.video-response').get(0).play();
              $("#record").blur();
            });

          textCheck = "";
        }
        prevTextCheck = textCheck;
        coroverIosVoice();
      }, 200);
    }
  }

  // user cam access code
  var localStream;
  var userCam = document.querySelector("#user");

  // if (navigator.mediaDevices.getUserMedia) {
  //     navigator.mediaDevices.getUserMedia({
  //             video: {
  //                 height: {
  //                     min: 280,
  //                     max: 280
  //                 },
  //                 width: {
  //                     min: 480,
  //                     max: 480
  //                 },
  //             },
  //             // video: true
  //         })
  //         .then(function(stream) {
  //             userCam.srcObject = stream;
  //             localStream = stream
  //         })
  //         .catch(function(err0r) {
  //             // console.log("Something went wrong!");
  //         });
  // }

  // user & bot screen minmax

  function userSpeak() {
    $("#user").show();
    $("#bot").addClass("smallScreen");
    $("#user").removeClass("smallScreenUser");
    // $('#closeUserCam').hide();
  }

  function botSpeak() {
    $("#user").addClass("smallScreenUser");
    $("#bot").removeClass("smallScreen");
    $("#bot").show();
    // console.log('bot speak');
  }

  function videoOff() {
    localStream.getVideoTracks()[0].stop();
    $("#videoOn").show();
    $("#videooff").hide();
  }

  function videoOn() {
    $("#videooff").show();
    $("#videoOn").hide();
    // if (navigator.mediaDevices.getUserMedia) {
    //     navigator.mediaDevices.getUserMedia({
    //             video: true
    //         })
    //         .then(function(stream) {
    //             userCam.srcObject = stream;
    //             localStream = stream
    //         })
    //         .catch(function(err0r) {
    //             // console.log("Something went wrong!");
    //         });
    // }
  }
  var firsttime = true;

  function open_ios_mic() {
    $(".medtel").addClass("hideThis");
    medtel_live = false;
    $(".mic-demon-container").addClass("hideThis");
    $("#record").focus();
    firsttime = false;
  }

  function ios_button() {
    if (firsttime === true) {
      medtel_live = false;
      $(".mic-demon-container").removeClass("hideThis");
      $(".popup").addClass("hideThis");
      $(".medtel").addClass("hideThis");
    } else {
      medtel_live = false;
      $("#record").focus();
      $(".popup").addClass("hideThis");
      $(".medtel").addClass("hideThis");
    }
  }
} else {
  // console.log(iosSafari);
  // console.log(isAndroid && isChrome);
  // console.log(!isMobile && isChrome);
}

$(document).ready(function () {
  $(".owl-carousel").owlCarousel({
    loop: true,
    autoplay: true,
    autoplayTimeout: 3000,
    autoplayHoverPause: true,
    nav: false,
    dots: false,
    responsiveClass: true,
    responsive: {
      0: {
        items: 5,
        margin: 10,
      },
      600: {
        items: 6,
        margin: 20,
      },
      1000: {
        items: 6,
        margin: 60,
      },
    },
  });
});

function startRecording() {
  navigator.mediaDevices
    .getUserMedia({
      video: false,
      audio: true,
    })
    .then(successCallback.bind(this), errorCallback.bind(this));
}

function successCallback(stream) {
  console.log("successCallback");
  localStem = stream;
  var options = {
    mimeType: "audio/mp3",
    numberOfAudioChannels: 1,
  };

  //Start Actuall Recording
  var StereoAudioRecorder = RecordRTC.StereoAudioRecorder;
  console.log(StereoAudioRecorder);
  recorder = new StereoAudioRecorder(stream, {
    mimeType: "audio/mp3",
    numberOfAudioChannels: 1,
  });
  recorder.record();
  //   var options = {
  //     mimeType: "audio/mp3",
  //     numberOfAudioChannels: 1,
  //   };
  //   recorder = RecordRTC(stream, options);
  //   recorder.startRecording();
}

function stopRecording(transcript) {
  localStem.stop();
  recorder.stop((blob) => {
    console.log(blob);

    voiceCapture(transcript, blob);
    // urls = [];
    // urls.push(URL.createObjectURL(blob));
  });

  //   isRecording = false;

  //   //   RecordRTC.stopRecording(function () {
  //   //     var blob = RecordRTC.getBlob(); // it'll NEVER be undefined
  //   //     console.log(blob);
  //   //   });
  //   localStem.stop();
  //   recorder.stop(processRecording.bind(this));
}
// SelectedQuery,mode,blob
function processRecording(blob, transcript) {
  console.log(transcript);
  console.log("blob");

  // this.chatbotService.voiceCapture(blob,)
  // console.log(blob)
  //   urls = [];
  //   urls.push(URL.createObjectURL(blob));
}
function errorCallback(error) {
  console.log("errror ");
  // this.error = 'Can not play audio in your browser';
}




$("#bot").on("oncanplay", function() {
  $("#bot").play();
});


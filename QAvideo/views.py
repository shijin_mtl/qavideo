from django.views import View
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
import json
import joblib
from django.http import JsonResponse





class VideoView(View):

    def get(self, *args, **kwargs):
        return render(self.request, 'index.html', {})



class DetectQuery(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(DetectQuery, self).dispatch(request, *args, **kwargs)

    def post(self, *args, **kwargs):
        data = self.request.POST
        query_dict =data.get("questionData")
        query_dict = json.loads(query_dict)
        filename = 'finalized_model_updated.sav'
        loaded_model = joblib.load(filename)
        pred=loaded_model.predict([query_dict["query"]])

        response_date = {
            "id":"61d7eabf-d42b-49ad-922c-49e4ca8e1cc8",
            "reply":"",
            "audio":"",
            "desktopVideo":"/static/video/"+str(pred[0])+".mp4",
            "mobileVideo":"/static/video/"+str(pred[0])+".mp4",
            "news":False
        }

        return JsonResponse(response_date)
        